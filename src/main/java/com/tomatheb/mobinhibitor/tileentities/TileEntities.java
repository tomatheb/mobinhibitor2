package com.tomatheb.mobinhibitor.tileentities;

import com.tomatheb.mobinhibitor.reference.Reference;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class TileEntities {
    public static void preInit() {
        GameRegistry.registerTileEntity(TileDropMaker.class,
                new ResourceLocation(Reference.MOD_ID, Reference.Tile.DROP_MAKER));
        GameRegistry.registerTileEntity(TilePersistMaker.class,
                new ResourceLocation(Reference.MOD_ID, Reference.Tile.LIVING_MAKER));
    }
}
