package com.tomatheb.mobinhibitor.reference;

public class Reference {
    public static final String MOD_NAME = "Mob Inhibitor";
    public static final String MOD_ID = "mobinhibitor";
    public static final String VERSION = "1.7.2";
    public static final String ROOT = "com.tomatheb.mobinhibitor.";
    public static final String CONFIG_LANG = "mobinhibitor.config.title";

    public final class Blocks {
        public static final String DROP_MAKER = "mob_drop_table";
        public static final String LIVING_MAKER = "mob_existence_crystal";
    }

    public final class Tile {
        public static final String DROP_MAKER = "tile_mob_drop_table";
        public static final String LIVING_MAKER = "tile_mob_existence_crystal";
    }
}


