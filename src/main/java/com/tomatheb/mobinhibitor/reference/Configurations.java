package com.tomatheb.mobinhibitor.reference;

import com.tomatheb.mobinhibitor.MobInhibitor;
import com.tomatheb.mobinhibitor.registries.SpecialCaseRegistry;
import net.minecraftforge.common.config.Config;
import net.minecraftforge.common.config.ConfigManager;
import net.minecraftforge.fml.client.event.ConfigChangedEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

@Config(modid = Reference.MOD_ID)
@Config.LangKey(Reference.CONFIG_LANG)
public class Configurations {

    @Config.Comment("Set to false to allow all mob spawns")
    @Config.Name("enabled")
    public static boolean enabled = true;

    @Config.Comment("List of whitelisted mobs")
    @Config.Name("whitelistMobs")
    public static String[] mobWhitelist = {};

    @Config.Comment("Spawning Block Properties")
    public static final General general = new General();

    public static class General {
        @Config.Comment("Allows bosses to exist")
        @Config.Name("allowBoss")
        public boolean allowBoss = false;

        @Config.Comment("Enables the mob drop field block")
        @Config.Name("dropFieldEnable")
        @Config.RequiresMcRestart
        public boolean dropField = true;

        @Config.Name("persistFieldEnable")
        @Config.Comment("Enables the mob persist field block")
        @Config.RequiresMcRestart
        public boolean persistField = true;


        @Config.Comment("The distance which the mob drop/persist field extends around the block")
        @Config.Name("dropFieldDistance")
        @Config.RangeInt(min = 0, max = 30)
        public int dropDistance = 15;

    }

    @Mod.EventBusSubscriber(modid = Reference.MOD_ID)
    private static class ConfigurationHandler {

        @SubscribeEvent
        public static void onConfigurationChangedEvent(ConfigChangedEvent.OnConfigChangedEvent event) {

            if (event.getModID().equalsIgnoreCase(Reference.MOD_ID)) {
                ConfigManager.sync(Reference.MOD_ID, Config.Type.INSTANCE);

                MobInhibitor.whitelistRegistry.reset();
                MobInhibitor.whitelistRegistry.bulkRegisterByName(Configurations.mobWhitelist);
            }
        }
    }
}
