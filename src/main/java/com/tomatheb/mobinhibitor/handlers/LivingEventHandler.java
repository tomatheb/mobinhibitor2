package com.tomatheb.mobinhibitor.handlers;

import com.mojang.authlib.GameProfile;
import com.tomatheb.mobinhibitor.reference.Configurations;
import net.minecraft.util.DamageSource;
import net.minecraft.world.WorldServer;
import net.minecraftforge.common.util.FakePlayerFactory;
import net.minecraftforge.event.entity.living.LivingEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

import java.util.UUID;

public class LivingEventHandler {

    private static final String FAKE_PLAYER_NAME = "[MI_PLAYER]";
    private static final UUID FAKE_PLAYER_ID = null;

    @SubscribeEvent
    public void onLivingUpdate(LivingEvent.LivingUpdateEvent event)
    {

        if (!event.getEntity().world.isRemote && Configurations.enabled)
        {
            if (!MobCheckHandler.shouldAllowPersist(event.getEntity(), event.getEntity().world)) {
                if (MobCheckHandler.shouldStillDrop(event.getEntity(), event.getEntity().world))
                {
                    //LogHelper.info("MobShouldDrop");
                    event.getEntity().attackEntityFrom(
                            DamageSource.causePlayerDamage(FakePlayerFactory.get((WorldServer) event.getEntity().world,
                                    new GameProfile(FAKE_PLAYER_ID, FAKE_PLAYER_NAME))),
                            6000);
                } else {
                    event.getEntity().setDead();
                }
            }
        }
    }
}
