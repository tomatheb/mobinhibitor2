package com.tomatheb.mobinhibitor.handlers;

import com.tomatheb.mobinhibitor.reference.Configurations;
import net.minecraftforge.event.entity.living.LivingSpawnEvent;
import net.minecraftforge.fml.common.eventhandler.Event;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class LivingSpawnHandler
{
    @SubscribeEvent
    public void onLivingSpawn(LivingSpawnEvent.CheckSpawn event)
    {
        if (!event.getEntity().world.isRemote && Configurations.enabled)
        {
            if (!MobCheckHandler.shouldAllowSpawn(event.getEntity(), event.getEntity().world)) {
                if (!MobCheckHandler.shouldStillDrop(event.getEntity(), event.getEntity().world)) {
                    event.setResult(Event.Result.DENY);
                }
            }
        }
        //event.setResult(Event.Result.DEFAULT);
    }
}
