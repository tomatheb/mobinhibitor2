package com.tomatheb.mobinhibitor.handlers;

import com.tomatheb.mobinhibitor.MobInhibitor;
import com.tomatheb.mobinhibitor.blocks.DropMaker;
import com.tomatheb.mobinhibitor.blocks.PersistMaker;
import com.tomatheb.mobinhibitor.reference.Configurations;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EnumCreatureType;
import net.minecraft.entity.monster.IMob;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import java.util.ArrayList;

public class MobCheckHandler {
    public static Boolean shouldAllowSpawn(Entity entity, World world) {
        if ((entity instanceof IMob || entity.isCreatureType(EnumCreatureType.MONSTER, false) ||
                MobInhibitor.specialCaseRegistry.isSpecialCaseByEntity(entity)) &&
                !MobInhibitor.whitelistRegistry.isWhitelistedByEntity(entity)) {

            if (!Configurations.general.allowBoss || entity.isNonBoss()) {
                if (!Configurations.general.persistField) {
                    return false;
                }
                BlockPos pos = entity.getPosition();
                int x = pos.getX();
                int y = pos.getY();
                int z = pos.getZ();
                int distanceSq = Configurations.general.dropDistance * Configurations.general.dropDistance;
                ArrayList<TileEntity> TEs = new ArrayList<>(world.loadedTileEntityList);
                for (TileEntity i : TEs) {
                    if (i.getBlockType() instanceof PersistMaker && i.getPos().distanceSqToCenter(x, y, z) < distanceSq) {
                        return true;
                    }
                }
                return false;
            }
        }
        return true;
    }

    public static Boolean shouldAllowPersist(Entity entity, World world) {

        if ((entity instanceof IMob || entity.isCreatureType(EnumCreatureType.MONSTER, false) ||
                MobInhibitor.specialCaseRegistry.isSpecialCaseByEntity(entity)) &&
                !MobInhibitor.whitelistRegistry.isWhitelistedByEntity(entity)) {
            //LogHelper.info(world.loadedTileEntityList);
            if (!Configurations.general.allowBoss || entity.isNonBoss()) {
                //If should be dealt with,
                if (!Configurations.general.persistField) {
                    // Kill if no persist field
                    return false;
                }
                BlockPos pos = entity.getPosition();
                int x = pos.getX();
                int y = pos.getY();
                int z = pos.getZ();
                int distanceSq = Configurations.general.dropDistance * Configurations.general.dropDistance;
                ArrayList<TileEntity> TEs = new ArrayList<>(world.loadedTileEntityList);
                for (TileEntity i : TEs) {
                    if (i.getBlockType() instanceof PersistMaker && i.getPos().distanceSqToCenter(x, y, z) < distanceSq) {
                        return true;
                    }
                }
                return false;
            }
        }
        return true;
    }

    public static Boolean shouldStillDrop(Entity entity, World world) {
        if (!Configurations.general.dropField) {
            return false;
        }
        BlockPos pos = entity.getPosition();
        int x = pos.getX();
        int y = pos.getY();
        int z = pos.getZ();
        int distanceSq = Configurations.general.dropDistance * Configurations.general.dropDistance;
        for (TileEntity i : world.loadedTileEntityList) {
            if (i.getBlockType() instanceof DropMaker && i.getPos().distanceSqToCenter(x, y, z) < distanceSq) {
                return true;
            }
        }
        return false;
    }
}
