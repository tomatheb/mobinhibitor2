package com.tomatheb.mobinhibitor;

import com.tomatheb.mobinhibitor.helper.LogHelper;
import com.tomatheb.mobinhibitor.proxy.IProxy;
import com.tomatheb.mobinhibitor.reference.Configurations;
import com.tomatheb.mobinhibitor.reference.Reference;
import com.tomatheb.mobinhibitor.registries.SpecialCaseRegistry;
import com.tomatheb.mobinhibitor.registries.WhitelistRegistry;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

@Mod(modid = Reference.MOD_ID, name=Reference.MOD_NAME, version = Reference.VERSION,
        acceptedMinecraftVersions = "1.12.2",
        dependencies = "required-before:autoreglib;")
public class MobInhibitor
{
    @Mod.Instance(Reference.MOD_ID)
    public static MobInhibitor instance;

    public static SpecialCaseRegistry specialCaseRegistry = new SpecialCaseRegistry();
    public static WhitelistRegistry whitelistRegistry = new WhitelistRegistry();

    @SidedProxy(clientSide=Reference.ROOT+"proxy.ClientProxy", serverSide=Reference.ROOT+"proxy.ServerProxy")
    public static IProxy proxy;

    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event)
    {
        //ConfigurationHandler.init(event.getSuggestedConfigurationFile());
        //MinecraftForge.EVENT_BUS.register(new ConfigurationHandler());
        LogHelper.init(event.getModLog());
        proxy.preInit();
    }

    @Mod.EventHandler
    public void init(FMLInitializationEvent event)
    {
        MinecraftForge.EVENT_BUS.register(instance);
        proxy.registerEventHandlers();
        proxy.init();
    }

    @Mod.EventHandler
    public void postInit(FMLInitializationEvent event)
    {
        //Register mod special cases:
        //Should be no longer needed as of hermitquest 1.1.23 (hopefully?)
        if (Loader.isModLoaded("hermitquest"))
        {
            specialCaseRegistry.registerSpecialCaseByName("hermitquest.diabloelpollo");
        }
        whitelistRegistry.bulkRegisterByName(Configurations.mobWhitelist);
    }
}
