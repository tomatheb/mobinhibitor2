package com.tomatheb.mobinhibitor.proxy;

import com.tomatheb.mobinhibitor.blocks.ModBlocks;
import com.tomatheb.mobinhibitor.handlers.LivingEventHandler;
import com.tomatheb.mobinhibitor.handlers.LivingSpawnHandler;
import com.tomatheb.mobinhibitor.handlers.PlayerInteractHandler;
import com.tomatheb.mobinhibitor.helper.LogHelper;
import com.tomatheb.mobinhibitor.reference.Configurations;
import com.tomatheb.mobinhibitor.tileentities.TileEntities;
import net.minecraftforge.common.MinecraftForge;
import vazkii.arl.recipe.RecipeHandler;
import vazkii.arl.util.ProxyRegistry;

public class CommonProxy implements IProxy
{
    public void registerEventHandlers()
    {
        LivingEventHandler livingEventHandler = new LivingEventHandler();
        LivingSpawnHandler livingSpawnHandler = new LivingSpawnHandler();
        PlayerInteractHandler playerInteractHandler = new PlayerInteractHandler();

        MinecraftForge.EVENT_BUS.register(livingEventHandler);
        MinecraftForge.EVENT_BUS.register(livingSpawnHandler);
        MinecraftForge.EVENT_BUS.register(playerInteractHandler);
    }

    public void init()
    {
        //NOOP
    }

    public void preInit()
    {
        ModBlocks.createBlocks();
        TileEntities.preInit();

        if (Configurations.general.dropField) {
            LogHelper.info("Drop field recipe enabled");
            RecipeHandler.addOreDictRecipe(ProxyRegistry.newStack(ModBlocks.dropMaker, 1),
                    "DID", "IEI", "DID",
                    'D', "gemDiamond",
                    'I', "ingotIron",
                    'E', "gemEmerald");
        }
        if (Configurations.general.persistField) {
            LogHelper.info("Persist field recipe enabled");
            RecipeHandler.addOreDictRecipe(ProxyRegistry.newStack(ModBlocks.persistMaker, 1),
                    "ISI", "SLS", "ISI",
                    'S', "stone",
                    'I', "ingotIron",
                    'L', "gemLapis");
        }
    }
}
