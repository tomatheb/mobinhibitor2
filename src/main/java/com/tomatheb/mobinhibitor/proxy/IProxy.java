package com.tomatheb.mobinhibitor.proxy;

public interface IProxy {
    public abstract void registerEventHandlers();
    public abstract void init();
    public abstract void preInit();
}
