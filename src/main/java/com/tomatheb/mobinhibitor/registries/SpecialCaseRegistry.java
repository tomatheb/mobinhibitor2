package com.tomatheb.mobinhibitor.registries;

import com.tomatheb.mobinhibitor.helper.LogHelper;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityList;

import java.util.ArrayList;
import java.util.List;

public class SpecialCaseRegistry
{
    private List<String> hostiles = new ArrayList<String>();

    public void registerSpecialCaseByName(String mobID)
    {
        LogHelper.info("Mob added to special case: "+mobID);
        hostiles.add(mobID);
    }


    public boolean isSpecialCaseByName(String mobID)
    {
        return hostiles.contains(mobID);
    }

    public boolean isSpecialCaseByEntity(Entity entity)
    {
        return hostiles.contains(EntityList.getEntityString(entity));
    }
}
