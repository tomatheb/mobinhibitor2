package com.tomatheb.mobinhibitor.registries;

import com.tomatheb.mobinhibitor.helper.LogHelper;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityList;

import java.util.ArrayList;
import java.util.List;

public class WhitelistRegistry {
    private List<String> hostiles = new ArrayList<>();

    public void registerWhitelistedByName(String mobID) {
        if (mobID.split("\\.")[0].toLowerCase().equals("minecraft")) {
            mobID = mobID.split("\\.")[1];
        }
        if (!isWhitelistedByName(mobID)) {
            LogHelper.info("Mob added to special case: " + mobID);
            hostiles.add(mobID.toLowerCase());
        }
    }


    public boolean isWhitelistedByName(String mobID) {
        return hostiles.contains(mobID.toLowerCase());
    }

    public boolean isWhitelistedByEntity(Entity entity) {
        try {
            return hostiles.contains(EntityList.getEntityString(entity).toLowerCase());
        } catch (NullPointerException e) {
            return false;
        }
    }

    public void reset() {
        LogHelper.info("Whitelist Registry Reloaded");
        hostiles.clear();
    }

    public void bulkRegisterByName(String[] list) {
        for (String element : list) {
            registerWhitelistedByName(element);
        }

    }

}
