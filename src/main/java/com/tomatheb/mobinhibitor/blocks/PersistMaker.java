package com.tomatheb.mobinhibitor.blocks;

import com.tomatheb.mobinhibitor.reference.Reference;
import com.tomatheb.mobinhibitor.tileentities.TileDropMaker;
import com.tomatheb.mobinhibitor.tileentities.TilePersistMaker;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import vazkii.arl.block.BlockModContainer;

public class PersistMaker extends BlockModContainer implements IMobInhibBlock {

    public boolean isBlockContainer;

    public PersistMaker(String unlocalizedName, Material material, float hardness, float resistance) {
        super(unlocalizedName, material);
        this.isBlockContainer = true;
        this.blockProperties(hardness, resistance);
    }

    public PersistMaker() {
        super(Reference.Blocks.LIVING_MAKER, Material.IRON);
        this.isBlockContainer = true;
        this.blockProperties(5.0f, 25.0f);
    }

    private void blockProperties(float hardness, float resistance) {
        this.setCreativeTab(CreativeTabs.COMBAT);
        this.setHardness(hardness);
        this.setResistance(resistance);
    }


    public TileEntity createNewTileEntity(World worldIn, IBlockState state) {
        return new TilePersistMaker();
    }

    @Override
    public TileEntity createNewTileEntity(World worldIn, int meta) {
        return new TileDropMaker();
    }
}
