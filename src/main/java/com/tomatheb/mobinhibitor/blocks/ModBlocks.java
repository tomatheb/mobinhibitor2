package com.tomatheb.mobinhibitor.blocks;

import com.tomatheb.mobinhibitor.helper.LogHelper;
import net.minecraft.block.Block;


public final class ModBlocks
{
    public static Block dropMaker;
    public static Block persistMaker;

    public static void createBlocks()
    {
        try {
            dropMaker = new DropMaker();
            //dropMaker.setCreativeTab(CreativeTabs.COMBAT);
            persistMaker = new PersistMaker();
            //persistMaker.setCreativeTab(CreativeTabs.COMBAT);
        } catch (NoSuchFieldError e) {
            LogHelper.fatal(e.getMessage());
            e.printStackTrace();
        }
    }
}
