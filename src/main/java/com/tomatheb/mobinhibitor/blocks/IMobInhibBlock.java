package com.tomatheb.mobinhibitor.blocks;

import com.tomatheb.mobinhibitor.reference.Reference;
import vazkii.arl.interf.IModBlock;

public interface IMobInhibBlock extends IModBlock {

    @Override
    default String getModNamespace() {
        return Reference.MOD_ID;
    }
}
