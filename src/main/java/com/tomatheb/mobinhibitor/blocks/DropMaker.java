package com.tomatheb.mobinhibitor.blocks;

import com.tomatheb.mobinhibitor.reference.Reference;
import com.tomatheb.mobinhibitor.tileentities.TileDropMaker;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import vazkii.arl.block.BlockModContainer;

public class DropMaker extends BlockModContainer implements IMobInhibBlock
{
    public boolean isBlockContainer;

    public DropMaker(String unlocalizedName, Material material, float hardness, float resistance) {
        super(unlocalizedName, material);
        this.isBlockContainer = true;
        this.blockProperties(hardness, resistance);
    }

    public DropMaker()
    {
        super(Reference.Blocks.DROP_MAKER, Material.IRON);
        this.isBlockContainer = true;
        this.blockProperties(5.0f, 25.0f);
    }


    private void blockProperties(float hardness, float resistance) {
        this.setCreativeTab(CreativeTabs.COMBAT);
        this.setHardness(hardness);
        this.setResistance(resistance);
    }

    @Override
    public TileEntity createNewTileEntity(World worldIn, int meta) {
        return new TileDropMaker();
    }

    public TileEntity createTileEntity(World worldIn, IBlockState state) {
        return new TileDropMaker();
    }
}
